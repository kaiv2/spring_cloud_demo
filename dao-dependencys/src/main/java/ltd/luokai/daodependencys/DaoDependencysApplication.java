package ltd.luokai.daodependencys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DaoDependencysApplication {

    public static void main(String[] args) {
        SpringApplication.run(DaoDependencysApplication.class, args);
    }
}
