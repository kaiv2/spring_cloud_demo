package ltd.luokai.servicedependencys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceDependencysApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceDependencysApplication.class, args);
    }
}
