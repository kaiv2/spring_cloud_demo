package ltd.luokai.server.zipkin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import zipkin.server.EnableZipkinServer;

/**
 * 最新 zipkin 服务
 * @see https://zipkin.io/
 */
@SpringBootApplication
@EnableZipkinServer
@Deprecated
public class ServerZipkinApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServerZipkinApplication.class, args);
	}
}
