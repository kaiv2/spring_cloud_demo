package ltd.luokai.gateway.server;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class ServerGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServerGatewayApplication.class, args);
	}

	@Bean
	public RouteLocator myRouteLocator(RouteLocatorBuilder builder) {
		return builder.routes()
				.route("service-a", r -> r.path("/api-a/**")
					.uri("http://localhost:8762"))
				.route("service-a", r -> r.path("/api-b/**")
					.filters(f -> f.rewritePath("/api-b","/"))
					.uri("http://localhost:8763"))
				.build();
	}

}
