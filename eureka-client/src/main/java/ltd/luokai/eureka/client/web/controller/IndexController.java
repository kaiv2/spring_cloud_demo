package ltd.luokai.eureka.client.web.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class IndexController {

	@Value("${server.port}")
	private String port;

	@Value("${foo}")
	private String foo;
	
	@RequestMapping("/hi")
	public String home(@RequestParam String name) {
		return "hi " + name + ", i am from port: " + port;
	}
	
	@PostMapping("/hello")
	public String hello(@RequestParam String name) {
		return "hello " + name;
	}
	
	@RequestMapping("/test")
	public String test() {
		return foo;
	}
}
