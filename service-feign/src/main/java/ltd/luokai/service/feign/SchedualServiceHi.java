package ltd.luokai.service.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "service-hi")
public interface SchedualServiceHi {

	@GetMapping("/hi")
	public String sayHiFormClientOne(@RequestParam(value = "name")String name);
	
	@PostMapping("/hello")
	public String hello(@RequestParam(value = "name")String name);
}
