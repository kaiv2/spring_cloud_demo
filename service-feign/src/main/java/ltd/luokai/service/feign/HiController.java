package ltd.luokai.service.feign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HiController {

	@Autowired
	private SchedualServiceHi schedualServiceHi;
	
	@GetMapping("/hi")
	public String sayHi(@RequestParam String name) {
		return schedualServiceHi.sayHiFormClientOne(name);
	}
	
	@PostMapping("/hello")
	public String hello(@RequestParam String name) {
		return schedualServiceHi.hello(name);
	}
}
